
function drawTable() {
    let gridBody = document.getElementById('gridBody');
    gridBody.style.width = gridProps.gridSize * gridProps.cellSize;
    gridBody.style.height = gridProps.gridSize * gridProps.cellSize;
    for(let i = 0; i < gridProps.gridSize; i++) {
        for(let j = 0; j < gridProps.gridSize; j++) {
            let newTile = document.createElement('div');
            newTile.classList.add("tiles");
            newTile.addEventListener("click", whichTileClicked);
            newTile.style.height = gridProps.cellSize;
            newTile.style.width = gridProps.cellSize;
            newTile.style.left = j*gridProps.cellSize;
            newTile.style.top = i*gridProps.cellSize;
            newTile.id = "tile" + j + "-" + i;
            gridBody.appendChild(newTile);
        }
    }
}

function placeEntitiesCorrectly() {
    let resetDiv = document.getElementById('resetButton');
    resetDiv.style.left = gridProps.gridSize * gridProps.cellSize + 50;

    let playDiv = document.getElementById('playButton');
    playDiv.style.left = gridProps.gridSize * gridProps.cellSize + 150;

    let winnerDiv = document.getElementById('winner');
    winnerDiv.style.left = gridProps.gridSize * gridProps.cellSize + 50;
    winnerDiv.innerText = "";

    let inputGridSizeDiv = document.getElementById('inputGridSize');
    inputGridSizeDiv.style.left = gridProps.gridSize * gridProps.cellSize + 50;

    let inputWinningNumberDiv = document.getElementById('inputWinningNumber');
    inputWinningNumberDiv.style.left = gridProps.gridSize * gridProps.cellSize + 50;
}

function whichTileClicked() {
    let tileId = this.id;
    //console.log(`Som v whichTileClicked a tileId = ${tileId}`);
    let tileCoordsString = tileId.substring(4);
    let tileCoordsArray = tileCoordsString.split('-');
    let column = parseInt(tileCoordsArray[0]);
    let row = parseInt(tileCoordsArray[1]);
    //console.log(`column = ${column} a row = ${row}`);
    clickOnTile(column, row);
}

function drawGameState() {
    //console.log(gameState);
    Array.from(document.getElementsByClassName("cross")).forEach(function(e) {
        e.classList.remove("cross");
    });
    Array.from(document.getElementsByClassName("circle")).forEach(function(e) {
        e.classList.remove("circle");
    });

    gameState.forEach(function(columnArray, column) {
        columnArray.forEach(function(state, row) {
            if(state !== tileState.EMPTY) {
                document.getElementById(`tile${column}-${row}`).classList.add((state == tileState.CROSS)?"cross":"circle");
                document.getElementById(`tile${column}-${row}`).style.backgroundSize = `${gridProps.cellSize - 1}px ${gridProps.cellSize - 1}px`;
            }
        });
    });
}

function displayPlayersTurn() {
    if (whichPlayersTurn == player.CROSS) {
        document.getElementById("player-label-cross").classList.add("active");
        document.getElementById("player-label-circle").classList.remove("active");
    } else {
        document.getElementById("player-label-circle").classList.add("active");
        document.getElementById("player-label-cross").classList.remove("active");
    }
}

function initHandlers() {
    let resetButton = document.getElementById('resetButton');
    resetButton.addEventListener('click', startNewGame);

    let playButtton = document.getElementById('playButton');
    playButtton.addEventListener('click', function() {
        updateDisplayState(STATE.playing);
    });
}

function displayWinner(whichPlayerWon) {
    if (whichPlayerWon == player.CROSS) {
        document.getElementById('winner').innerText = "Vyhral X";
    } else if(whichPlayerWon == player.CIRCLE) {
        document.getElementById('winner').innerText = "Vyhral O";
    }
}

function updateDisplayState(currentState) {
    let tiles;

    switch (currentState) {
        case STATE.editing:
            document.getElementById('winner').innerText = "";

            document.getElementById('playButton').style.display = "block";

            document.getElementById('inputGridSize').style.display = "block";
            document.getElementById('inputWinningNumber').style.display = "block";

            tiles = Array.from(document.getElementsByClassName('tiles'));
            tiles.forEach(function(element) {
                element.removeEventListener('click', whichTileClicked);
            });

            document.getElementById("player-label-cross").classList.remove("active");
            document.getElementById("player-label-circle").classList.remove("active");
            break;
    
        case STATE.playing:
            drawTable();

            document.getElementById('playButton').style.display = "none";
            document.getElementById('inputGridSize').style.display = "none";
            document.getElementById('inputWinningNumber').style.display = "none";

            document.getElementById("player-label-cross").classList.add("active");
            document.getElementById("player-label-circle").classList.remove("active");
            break;
        
        case STATE.gameWon:
            tiles = Array.from(document.getElementsByClassName('tiles'));
            tiles.forEach(function(element) {
                element.removeEventListener('click', whichTileClicked);
            });
            break;
    }
}