
let whichPlayersTurn;
let gameState = [];

const WINNING_LIMIT = 3;

const gridProps = {
    gridSize: 3,
    cellSize: 80
}
const tileState = {
    EMPTY: "empty",
    CROSS: "cross",
    CIRCLE: "circle"
}
const player = {
    CROSS: 1,
    CIRCLE: 2
}
const STATE = {
    editing: 1,
    playing: 2,
    gameWon: 3
}

const DIRECTION = {
    up: {rowChange: -1, columnChange: 0},
    down: {rowChange: 1, columnChange: 0},
    left: {rowChange: 0, columnChange: -1},
    right: {rowChange: 0, columnChange: 1},
    upRight: {rowChange: -1, columnChange: 1},
    downRight: {rowChange: 1, columnChange: 1},
    downLeft: {rowChange: 1, columnChange: -1},
    upLeft: {rowChange: -1, columnChange: -1}
}

function generateGame() {
    gameState = [];
    
    for(let i = 0; i < gridProps.gridSize; i++) {
        let column = [];
        for(let j = 0; j < gridProps.gridSize; j++) {
            column.push(tileState.EMPTY);
        }
        gameState.push(column);
    }
    whichPlayersTurn = player.CROSS;
}

function clickOnTile(column, row) {
    if(gameState[column][row] !== tileState.EMPTY) {
        return;
    }
    if(whichPlayersTurn == player.CROSS) {
        gameState[column][row] = tileState.CROSS;
    } else {
        gameState[column][row] = tileState.CIRCLE;
    }
    drawGameState();

    if (isThereWinner(column, row)) {
        displayWinner(whichPlayersTurn);
        updateDisplayState(STATE.gameWon);
        return;
    } else {
        switchPlayer()
    }
    console.log(whichPlayersTurn);
}

function switchPlayer() {
    if (whichPlayersTurn == player.CROSS) {
        whichPlayersTurn = player.CIRCLE;
    } else {
        whichPlayersTurn = player.CROSS;
    }
    displayPlayersTurn();
}

function isThereWinner(column, row) {

    if (1 + countSymbolsInOneDirection(column, row, DIRECTION.up) + countSymbolsInOneDirection(column, row, DIRECTION.down) >= WINNING_LIMIT) {
        return true;
    } else if (1 + countSymbolsInOneDirection(column, row, DIRECTION.upRight) + countSymbolsInOneDirection(column, row, DIRECTION.downLeft) >= WINNING_LIMIT) {
        return true;
    } else if (1 + countSymbolsInOneDirection(column, row, DIRECTION.right) + countSymbolsInOneDirection(column, row, DIRECTION.left) >= WINNING_LIMIT) {
        return true;
    } else if (1 + countSymbolsInOneDirection(column, row, DIRECTION.downRight) + countSymbolsInOneDirection(column, row, DIRECTION.upLeft) >= WINNING_LIMIT) {
        return true;
    }
}

function countSymbolsInOneDirection(explorerColumn, explorerRow, direction) {

    let counter = 0;
    let shouldContinue;
 
    do {
        explorerRow = explorerRow + direction.rowChange;
        explorerColumn = explorerColumn + direction.columnChange;
        shouldContinue = false;
        if(tileExists(explorerColumn, explorerRow) && (((whichPlayersTurn == player.CROSS) && (gameState[explorerColumn][explorerRow] == tileState.CROSS)) || ((whichPlayersTurn == player.CIRCLE) && (gameState[explorerColumn][explorerRow] == tileState.CIRCLE)))) {
            counter++;
            shouldContinue = true;
        }
    } while(shouldContinue);
    //console.log(`Counter = ${counter}`);
    return counter;
}


function tileExists(columnToCheck, rowToCheck) {
    if (columnToCheck < 0 || rowToCheck < 0 || columnToCheck >= gridProps.gridSize || rowToCheck >= gridProps.gridSize) {
        return false;
    } else {
        return true;
    }
}

function startNewGame() {
    generateGame();
    drawTable();
    placeEntitiesCorrectly();
    drawGameState();
    updateDisplayState(STATE.editing);
}

function startGame() {
    initHandlers();
    startNewGame();
    
}